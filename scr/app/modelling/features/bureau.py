import pandas as pd
import numpy as np
from config import DB_ARGS
from SQL_get_send_functions import get_df_from_query

# получение данных
bureau = get_df_from_query(
    """SELECT * FROM bureau""",
    DB_ARGS
)

bureau_balance = get_df_from_query(
    """SELECT * FROM bureau_balance""",
    DB_ARGS
)

bureau.columns = [x.upper() for x in bureau.columns]
bureau_balance.columns = [x.upper() for x in bureau_balance.columns]

# дата фрейм с новыми признаками
bureau_features = pd.DataFrame(index=bureau['SK_ID_CURR'].unique())

# количество кредитов
bureau_features['all_credits'] = bureau[['SK_ID_CURR', 'SK_ID_BUREAU']].groupby(
    ['SK_ID_CURR'])['SK_ID_BUREAU'].count()

# количество закрытых кредитов
bureau['closed_credit'] = np.where(bureau['CREDIT_ACTIVE'] == 'Closed', 1,0)
bureau_features['closed_credit'] = bureau[['SK_ID_CURR', 'closed_credit']].groupby(
    ['SK_ID_CURR'])['closed_credit'].sum()

# количество открытых кредитов
bureau['active_credit'] = np.where(bureau['CREDIT_ACTIVE'] == 'Active', 1,0)
bureau_features['opened_credit'] = bureau[['SK_ID_CURR', 'active_credit']].groupby(
    ['SK_ID_CURR'])['active_credit'].sum()

# доля открытых кредитов
bureau_features['rat_closed_credit'] = bureau_features['closed_credit'] / bureau_features['all_credits']

# доля закрытых кредитов
bureau_features['rat_closed_credit'] = bureau_features['opened_credit'] / bureau_features['all_credits']

# количество просроченных кредитов по разному количеству дней просрочки
status_oh = pd.get_dummies(data = bureau_balance['STATUS'], columns = ['STATUS'], prefix='STATUS')
status_oh_cols = list(status_oh.columns)
bureau_balance[status_oh_cols] = status_oh

bureau_features[status_oh_cols] = bureau[['SK_ID_CURR','SK_ID_BUREAU']].merge(
    bureau_balance.groupby(
        ['SK_ID_BUREAU'],
        as_index=False)[status_oh_cols].max(),
        on = ['SK_ID_BUREAU'],
        how = 'left')[['SK_ID_CURR'] + status_oh_cols].groupby(
            ['SK_ID_CURR'], as_index = False).sum().set_index('SK_ID_CURR')

# доля кредитов по разному количеству дней просрочки
bureau_features['rat_' + pd.Index(status_oh_cols)] = bureau_features[pd.Index(status_oh_cols)].divide(
    bureau_features['all_credits'], axis=0)

# прошло дней с последнего закрытого кредита
bureau_features['last_closed_credit'] = bureau.loc[
    bureau['CREDIT_ACTIVE'] == 'Closed', ['SK_ID_CURR', 'DAYS_CREDIT']].groupby(['SK_ID_CURR'])['DAYS_CREDIT'].max()

# прошло дней с последнего открытого кредита
bureau_features['last_active_credit'] = bureau.loc[
    bureau['CREDIT_ACTIVE'] == 'Active', ['SK_ID_CURR', 'DAYS_CREDIT']].groupby(['SK_ID_CURR'])['DAYS_CREDIT'].max()

# максимальная сумма просрочки
bureau_features['max_sum_overdue'] = bureau[['SK_ID_CURR','AMT_CREDIT_SUM_OVERDUE']].groupby(
    ['SK_ID_CURR'])['AMT_CREDIT_SUM_OVERDUE'].max().fillna(0)

# минимальная сумма просрочки
bureau_features['min_sum_overdue'] = bureau[['SK_ID_CURR','AMT_CREDIT_SUM_OVERDUE']].groupby(
    ['SK_ID_CURR'])['AMT_CREDIT_SUM_OVERDUE'].min().fillna(0)

# доля общей суммы просрочки на сумму кредита
bureau_features['rat_sum_overdue'] = bureau[
    ['SK_ID_CURR','AMT_CREDIT_SUM_OVERDUE']].groupby(['SK_ID_CURR'])['AMT_CREDIT_SUM_OVERDUE'].sum().fillna(0) / bureau[
        ['SK_ID_CURR','AMT_CREDIT_SUM']].groupby(['SK_ID_CURR'])['AMT_CREDIT_SUM'].sum()

# доля общей суммы просрочки на сумму кредита
bureau_features['rat_sum_overdue'] = bureau[
    ['SK_ID_CURR','AMT_CREDIT_SUM_OVERDUE']].groupby(['SK_ID_CURR'])['AMT_CREDIT_SUM_OVERDUE'].sum().fillna(0) / bureau[
        ['SK_ID_CURR','AMT_CREDIT_SUM']].groupby(['SK_ID_CURR'])['AMT_CREDIT_SUM'].sum()

# количество кредитов определённого типа
credit_type = pd.get_dummies(data = bureau['CREDIT_TYPE'], columns = ['CREDIT_TYPE'], prefix='credit_type')
credit_type_cols = list(credit_type.columns)
bureau[credit_type_cols] = credit_type

bureau_features[credit_type.columns + 'count'] = bureau[['SK_ID_CURR'] + credit_type_cols].groupby(
    ['SK_ID_CURR'])[credit_type_cols].sum()

# количество просрочек кредитов определенного типа
bureau_features[credit_type.columns + 'overdue_count'] = bureau.loc[
    bureau['AMT_CREDIT_SUM_OVERDUE'] > 0, ['SK_ID_CURR'] + credit_type_cols].groupby(
        ['SK_ID_CURR'])[credit_type_cols].sum()

# количество закрытых кредитов определенного типа
bureau_features[credit_type.columns + 'closed_count'] = bureau.loc[
    bureau['CREDIT_ACTIVE'] == 'Closed', ['SK_ID_CURR'] + credit_type_cols].groupby(
        ['SK_ID_CURR'])[credit_type_cols].sum()

bureau_features.to_csv('data/bureau_features.csv')
