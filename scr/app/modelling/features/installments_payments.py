import pandas as pd
from config import DB_ARGS
from SQL_get_send_functions import get_df_from_query

# получение данных
installments_payments = get_df_from_query(
    """SELECT * FROM installments_payments""",
    DB_ARGS
)

installments_payments.columns = [x.upper() for x in installments_payments.columns]

installments_payments_features = pd.DataFrame(index=installments_payments['SK_ID_CURR'].unique())
cols = installments_payments.columns.drop(['SK_ID_CURR'])

for agg in ['min', 'max', 'mean', 'median', 'count', 'nunique']:
    temp = installments_payments.groupby(['SK_ID_CURR'])[cols].agg(agg)
    installments_payments_features[cols + '_' + agg] = temp

installments_payments_features.to_csv('data/installments_payments_features.csv')
