import pandas as pd
from config import DB_ARGS
from SQL_get_send_functions import get_df_from_query

# получение данных
previous_application = get_df_from_query(
    """SELECT * FROM previous_application""",
    DB_ARGS
)

previous_application.columns = [x.upper() for x in previous_application.columns]

# дата фрейм с новыми признаками
cols = pd.Index(previous_application.columns.drop(['SK_ID_CURR']))
previous_application_features = pd.DataFrame(index=previous_application['SK_ID_CURR'].unique())

cat_cols = previous_application.dtypes[previous_application.dtypes == 'object'].index
num_cols = previous_application.dtypes[previous_application.dtypes != 'object'].index

previous_application_features = pd.DataFrame(index=previous_application['SK_ID_CURR'].unique())

for agg in ['min', 'max', 'mean', 'median', 'count', 'nunique']:
    temp = previous_application.groupby(['SK_ID_CURR'])[num_cols].agg(agg)
    previous_application_features[num_cols + '_' + agg] = temp

temp = previous_application.groupby(['SK_ID_CURR'])[cat_cols].agg('nunique')
previous_application_features[cat_cols + '_' + 'nunique'] = temp

previous_application_features.to_csv('data/previous_application_features.csv')
