import pandas as pd
from config import DB_ARGS
from SQL_get_send_functions import get_df_from_query

# получение данных
credit_card_balance = get_df_from_query(
    """SELECT * FROM credit_card_balance""",
    DB_ARGS
)

credit_card_balance.columns = [x.upper() for x in credit_card_balance.columns]

# дата фрейм с новыми признаками
cols = pd.Index(credit_card_balance.columns.drop(['SK_ID_CURR', 'NAME_CONTRACT_STATUS']))
credit_card_balance_features = pd.DataFrame(index=credit_card_balance['SK_ID_CURR'].unique())

for agg in ['min', 'max', 'mean', 'median', 'count', 'nunique']:

    temp = credit_card_balance.groupby(['SK_ID_CURR'])[cols].agg(agg)
    credit_card_balance_features[cols + '_' + agg] = temp

    temp_3 = credit_card_balance[credit_card_balance['MONTHS_BALANCE'] >= -3].groupby(['SK_ID_CURR'])[cols].agg(agg)
    credit_card_balance_features[cols + '_rat_3_' + agg] = temp.divide(temp_3)

    temp_1 = credit_card_balance[credit_card_balance['MONTHS_BALANCE'] == -1].groupby(['SK_ID_CURR'])[cols].agg(agg)
    credit_card_balance_features[cols + '_rat_1_' + agg] = temp.divide(temp_1)

credit_card_balance_features.to_csv('data/credit_card_balance_features.csv')
