import pandas as pd
import numpy as np
from config import DB_ARGS
from SQL_get_send_functions import get_df_from_query

# получение данных
application = get_df_from_query(
    """SELECT * FROM application_train""",
    DB_ARGS
)

# объединение
application.columns = [x.upper() for x in application.columns]
application = application.set_index('SK_ID_CURR')

# дата фрейм с новыми признаками
application_features = pd.DataFrame(index=application.index)

# кол-во документов
doc_cols =  ['FLAG_DOCUMENT_2',
 'FLAG_DOCUMENT_3',
 'FLAG_DOCUMENT_4',
 'FLAG_DOCUMENT_5',
 'FLAG_DOCUMENT_6',
 'FLAG_DOCUMENT_7',
 'FLAG_DOCUMENT_8',
 'FLAG_DOCUMENT_9',
 'FLAG_DOCUMENT_10',
 'FLAG_DOCUMENT_11',
 'FLAG_DOCUMENT_12',
 'FLAG_DOCUMENT_13',
 'FLAG_DOCUMENT_14',
 'FLAG_DOCUMENT_15',
 'FLAG_DOCUMENT_16',
 'FLAG_DOCUMENT_17',
 'FLAG_DOCUMENT_18',
 'FLAG_DOCUMENT_19',
 'FLAG_DOCUMENT_20',
 'FLAG_DOCUMENT_21']

application_features['doc_counts'] = application[doc_cols].sum(axis = 1)

# информация о доме
home_cols =  ['EXT_SOURCE_1',
 'EXT_SOURCE_2',
 'EXT_SOURCE_3',
 'APARTMENTS_AVG',
 'BASEMENTAREA_AVG',
 'YEARS_BEGINEXPLUATATION_AVG',
 'YEARS_BUILD_AVG',
 'COMMONAREA_AVG',
 'ELEVATORS_AVG',
 'ENTRANCES_AVG',
 'FLOORSMAX_AVG',
 'FLOORSMIN_AVG',
 'LANDAREA_AVG',
 'LIVINGAPARTMENTS_AVG',
 'LIVINGAREA_AVG',
 'NONLIVINGAPARTMENTS_AVG',
 'NONLIVINGAREA_AVG',
 'APARTMENTS_MODE',
 'BASEMENTAREA_MODE',
 'YEARS_BEGINEXPLUATATION_MODE',
 'YEARS_BUILD_MODE',
 'COMMONAREA_MODE',
 'ELEVATORS_MODE',
 'ENTRANCES_MODE',
 'FLOORSMAX_MODE',
 'FLOORSMIN_MODE',
 'LANDAREA_MODE',
 'LIVINGAPARTMENTS_MODE',
 'LIVINGAREA_MODE',
 'NONLIVINGAPARTMENTS_MODE',
 'NONLIVINGAREA_MODE',
 'APARTMENTS_MEDI',
 'BASEMENTAREA_MEDI',
 'YEARS_BEGINEXPLUATATION_MEDI',
 'YEARS_BUILD_MEDI',
 'COMMONAREA_MEDI',
 'ELEVATORS_MEDI',
 'ENTRANCES_MEDI',
 'FLOORSMAX_MEDI',
 'FLOORSMIN_MEDI',
 'LANDAREA_MEDI',
 'LIVINGAPARTMENTS_MEDI',
 'LIVINGAREA_MEDI',
 'NONLIVINGAPARTMENTS_MEDI',
 'NONLIVINGAREA_MEDI',
 'FONDKAPREMONT_MODE',
 'HOUSETYPE_MODE',
 'TOTALAREA_MODE',
 'WALLSMATERIAL_MODE',
 'EMERGENCYSTATE_MODE']

application_features['home_inform'] = (application[home_cols].isna().sum(axis = 1) < 30).astype(int)

# полных лет
application_features['years_birth'] = application['DAYS_BIRTH'] // -365

# год смены документа
application_features['year_registration'] = application['DAYS_REGISTRATION'] // -365

# разница между возрастом и сменой документа
application_features['dif_yb_yr'] = application_features['years_birth'] - application_features['year_registration']

# задержка в смене документа
conds = [
    (application_features['years_birth'] > 45) & (application_features['dif_yb_yr'] < 45),
    (application_features['years_birth'] > 45) & (application_features['dif_yb_yr'] >= 45),
    (application_features['years_birth'] > 20) & (application_features['dif_yb_yr'] < 20),
    (application_features['years_birth'] > 20) & (application_features['dif_yb_yr'] >= 20),
    (application_features['years_birth'] > 14) & (application_features['dif_yb_yr'] < 14),
    (application_features['years_birth'] > 14) & (application_features['dif_yb_yr'] >= 14)
]

choises = [
    application_features['years_birth'] - 45,
    application_features['dif_yb_yr'] - 45,
    application_features['years_birth'] - 20,
    application_features['dif_yb_yr'] - 20,
    application_features['years_birth'] - 14,
    application_features['dif_yb_yr'] - 14,
]

application_features['delay_change_doc'] = np.select(conds, choises, 0)

# доля отдаваемого дохода
application_features['credit_income_rate'] = application['AMT_ANNUITY'] / application['AMT_INCOME_TOTAL']

# количество детей на одного взрослого
application_features['child_parents_ratio'] = application['CNT_CHILDREN'] / (
        application['CNT_FAM_MEMBERS'] - application['CNT_CHILDREN'])

# доход на одного ребёнка (с заменой inf)
application_features['income_child'] = application['AMT_INCOME_TOTAL'] / application['CNT_CHILDREN']

application_features['income_child'] = np.where(
    application_features['income_child'] == np.inf, application_features.loc[
        application_features['income_child'] != np.inf, 'income_child'].median(), application_features['income_child'])

# доход на одного взрослого (с заменой inf)
application_features['income_parents'] = application['AMT_INCOME_TOTAL'] / (
        application['CNT_FAM_MEMBERS'] - application['CNT_CHILDREN'])

application_features['income_parents'] = np.where(
    application_features['income_parents'] == np.inf,
    application_features.loc[application_features['income_child'] != np.inf, 'income_parents'].median(),
    application_features['income_parents'])

# процентная ставка по кредиту
application_features['credit_percent'] = (
    application['AMT_CREDIT'] - application['AMT_GOODS_PRICE']) / (application['AMT_GOODS_PRICE'] * application['AMT_CREDIT'] / application['AMT_ANNUITY'] / 12)

# процент кредита
application_features['credit_percent'] = (
    application['AMT_CREDIT'] - application['AMT_GOODS_PRICE']) / application['AMT_CREDIT'] * 100

# разница со средним доходом в группе по полу и образованию
temp = application.groupby(['CODE_GENDER', 'NAME_EDUCATION_TYPE'], as_index=False)['AMT_INCOME_TOTAL'].mean()
application_features['gend_educ_diff'] = application['AMT_INCOME_TOTAL'] - application[
    ['CODE_GENDER', 'NAME_EDUCATION_TYPE']].merge(
    temp, on=['CODE_GENDER', 'NAME_EDUCATION_TYPE'], how = 'left')['AMT_INCOME_TOTAL']

application_features.to_csv('data/application_features.csv')
