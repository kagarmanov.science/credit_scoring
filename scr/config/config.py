DB_USER = 'postgres'
DB_PASSWORD = 'postgres'
DATA_FULL_PATH = 'C:\\Temp\\data\\'
DATABASE_NAME = 'home_credit'
PORT = '5432'
HOST = 'localhost'

DB_ARGS = {
    'database': DATABASE_NAME,
    'host': HOST,
    'user': DB_USER,
    'password': DB_PASSWORD,
    'port' : PORT
}